package answer4;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class ColorComboBox extends JFrame {
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JButton btn;
	private JComboBox box;
	private JPanel panel1,panel2;
	private ActionListener listener;
	
	public ColorComboBox(){
		
		box = new JComboBox();
		box.addItem("Red");
		box.addItem("Green");
		box.addItem("Blue");
		
		btn = new JButton("OK");
		btn.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String n = ("Red");
					if(n == box.getSelectedItem()){
						panel1.setBackground(RED_COLOR);
					}
					String m = ("Green");
					if(m == box.getSelectedItem()){
						panel1.setBackground(GREEN_COLOR);
					}
					String o = ("Blue");
					if(o == box.getSelectedItem()){
						panel1.setBackground(BLUE_COLOR);
					}
					
				}
			}
		);
		
		
		
		panel1 = new JPanel();
		panel1.setBackground(RED_COLOR);
		panel2 = new JPanel(new GridLayout(1,2));
		
		panel2.add(box);
		panel2.add(btn);
		
		
		
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		
		
	}
	
}
