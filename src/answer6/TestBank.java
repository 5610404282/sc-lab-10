package answer6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import answer5.ColorMenu;

public class TestBank {
	BankAccount bank;
	BankGUI frame;
	ActionListener listener;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new TestBank();
		
	}
	public TestBank(){
		frame = new BankGUI();
		bank = new BankAccount(0);
		frame.setSize(400, 110);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("----- ANSWER 6 -----");
		frame.setVisible(true);
		
		frame.getBtnDeposit().addActionListener(listener);
		frame.getBtnWithdraw().addActionListener(listener);
		
		
		frame.setListener(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	double amount = Double.parseDouble(frame.getInput());
	        	bank.deposit(amount);
	        	frame.getLabel().setText("Balance : " + bank.getBalance());
	        	}});

		frame.setListener2(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	double amount = Double.parseDouble(frame.getInput());
	        	bank.withdraw(amount);
	        	frame.getLabel().setText("Balance : " + bank.getBalance());
	        	}});
	
	}   
    
}
