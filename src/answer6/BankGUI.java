package answer6;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BankGUI extends JFrame {

	   private JTextField amount;
	   private JButton deposit,withdraw;
	   private JLabel balance,amountLabel;
	   private JPanel panel,panel2;
	
	   public BankGUI(){
		   
		   amountLabel = new JLabel("Amount :  ");
		   amount = new JTextField(10);
		   
		   deposit = new JButton("Deposit");
		   withdraw = new JButton("Withdraw");
		   
		   balance = new JLabel("Balance : " + 0);
		   
		   panel = new JPanel();
		   panel2 = new JPanel();
		   
		   panel.add(amountLabel);
		   panel.add(amount);
		   panel.add(deposit);
		   panel.add(withdraw);
		   
		   panel2.add(balance);
		   add(panel,BorderLayout.NORTH);
		   add(panel2,BorderLayout.CENTER);
		   
	   }
	   
	   public String getInput(){
		   return amount.getText();
	   }
	   
	   public void setListener(ActionListener list){
			 deposit.addActionListener(list);
	   }
	   
	   public void setListener2(ActionListener list) {
			withdraw.addActionListener(list);
		}
	   
	   public JLabel getLabel(){
		   return balance;
	   }

	   public JButton getBtnDeposit() {
		   return deposit;
	   }
	   
	   public JButton getBtnWithdraw() {
		   return withdraw;
	   }
}
