package answer2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class ColorRadioButton extends JFrame {
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JPanel panel1,panel2;
	private JRadioButton btnRed,btnGreen,btnBlue;
	
	public ColorRadioButton(){
		createButton();
		
		panel1 = new JPanel();
		panel1.setLayout(null);
		panel2 = new JPanel(new GridLayout(1,3));
		
		panel2.add(btnRed);
		panel2.add(btnGreen);
		panel2.add(btnBlue);
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
	}

	private void createButton() {
		// TODO Auto-generated method stub
		
		btnRed = new JRadioButton("Red");
		btnRed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(RED_COLOR);
            }});
		
		btnGreen = new JRadioButton("Green");
		btnGreen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(GREEN_COLOR);
            }});
		
		btnBlue = new JRadioButton("Blue");
		btnBlue.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(BLUE_COLOR);
            }});
	
		
		ButtonGroup group = new ButtonGroup();
		group.add(btnRed);
		group.add(btnGreen);
		group.add(btnBlue);
		
	}
}
