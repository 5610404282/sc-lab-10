package answer5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class ColorMenu extends JFrame {
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JMenu set,color,file;
	private JMenuItem red,green,blue,exit;
	private JMenuBar menu;
	private JPanel panel;
	
	public ColorMenu(){
		file = new JMenu("File");
		set = new JMenu("Setting");
		color = new JMenu("Color");
		
		exit = new JMenuItem("Exit");
		red = new JMenuItem("Red");
		green = new JMenuItem("Green");
		blue = new JMenuItem("Blue");
		
		color.add(red);
		color.add(green);
		color.add(blue);
		set.add(color);
		file.add(exit);
		menu = new JMenuBar();
		menu.add(file);
		menu.add(set);
		setJMenuBar(menu);
		
		panel = new JPanel();
		add(panel,BorderLayout.CENTER);
		
		exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);	
            }});
		
		red.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel.setBackground(RED_COLOR);
            }});
		
		green.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel.setBackground(GREEN_COLOR);
            }});
		
		blue.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel.setBackground(BLUE_COLOR);
            }});

	}

}
