package answer3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ColorCheckBox extends JFrame {
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private final static Color YELLOW_COLOR = Color.YELLOW;
	private final static Color PINK_COLOR = Color.PINK;
	private final static Color CYAN_COLOR = Color.CYAN;
	private final static Color WHITE_COLOR = Color.WHITE;
	
	
	private JCheckBox btnRed,btnGreen,btnBlue;
	private JPanel panel1,panel2;
	private ActionListener listener;
	
	public ColorCheckBox(){
		
		class Listener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {

				if(btnRed.isSelected()){
					panel1.setBackground(RED_COLOR);
				}
				if(btnGreen.isSelected()){
					panel1.setBackground(GREEN_COLOR);
				}
				if(btnBlue.isSelected()){
					panel1.setBackground(BLUE_COLOR);
				}
				if(btnRed.isSelected() & btnGreen.isSelected()){
					panel1.setBackground(YELLOW_COLOR);
				}
				if(btnRed.isSelected() & btnBlue.isSelected()){
					panel1.setBackground(PINK_COLOR);
				}
				if(btnGreen.isSelected() & btnBlue.isSelected()){
					panel1.setBackground(CYAN_COLOR);
				}
				if(btnGreen.isSelected() & btnBlue.isSelected() & btnRed.isSelected()){
					panel1.setBackground(WHITE_COLOR);
				}
			}
		}
		listener = new Listener();

		btnRed = new JCheckBox("Red");
		btnRed.addActionListener(listener);
		btnGreen = new JCheckBox("Green");
		btnGreen.addActionListener(listener);
		btnBlue = new JCheckBox("Blue");
		btnBlue.addActionListener(listener);
		
		panel1 = new JPanel();
		
		panel2 = new JPanel(new GridLayout(1,3));
		
		panel2.add(btnRed);
		panel2.add(btnGreen);
		panel2.add(btnBlue);
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
	}

}
