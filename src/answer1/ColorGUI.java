package answer1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ColorGUI extends JFrame {
	
	private final static Color RED_COLOR = Color.RED;
	private final static Color GREEN_COLOR = Color.GREEN;
	private final static Color BLUE_COLOR = Color.BLUE;
	
	private JFrame frame;
	private JButton btnRed,btnGreen,btnBlue;
	private JPanel panel1,panel2;
	
	public ColorGUI(){
		createButton();
		
		panel1 = new JPanel();
		panel2 = new JPanel(new GridLayout(1,3));
		
		panel2.add(btnRed);
		panel2.add(btnGreen);
		panel2.add(btnBlue);
		
		add(panel1,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
	}

	private void createButton() {
		// TODO Auto-generated method stub
		btnRed = new JButton("Red");
		btnRed.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(RED_COLOR);
            }});
		btnGreen = new JButton("Green");
		btnGreen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(GREEN_COLOR);
            }});
		
		btnBlue = new JButton("Blue");
		btnBlue.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	panel1.setBackground(BLUE_COLOR);
            }});
	
		
	}
	
	
	
}


